<?php

class Mageapps_Calendar_Block_Adminhtml_Calendar_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('calendar_form', array('legend'=>Mage::helper('calendar')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('calendar')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
      $fieldset->addField('month', 'select', array(
          'label'     => Mage::helper('calendar')->__('Month'),
          'required'  => false,
          'name'      => 'month',
		  'values'    => array(
              array('value'     => '1','label'     => Mage::helper('calendar')->__('January'),),
			  array('value'     => '2','label'     => Mage::helper('calendar')->__('February'),),
			  array('value'     => '3','label'     => Mage::helper('calendar')->__('March'),),
			  array('value'     => '4','label'     => Mage::helper('calendar')->__('April'),),
              array('value'     => '5','label'     => Mage::helper('calendar')->__('May'),),
			  array('value'     => '6','label'     => Mage::helper('calendar')->__('June'),),
              array('value'     => '7','label'     => Mage::helper('calendar')->__('July'),),
			  array('value'     => '8','label'     => Mage::helper('calendar')->__('August'),),
              array('value'     => '9','label'     => Mage::helper('calendar')->__('September'),),
			  array('value'     => '10','label'     => Mage::helper('calendar')->__('October'),),
              array('value'     => '11','label'     => Mage::helper('calendar')->__('November'),),
			  array('value'     => '12','label'     => Mage::helper('calendar')->__('December'),),
          ),
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('calendar')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('calendar')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('calendar')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('calendar')->__('Content'),
          'title'     => Mage::helper('calendar')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getCalendarData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCalendarData());
          Mage::getSingleton('adminhtml/session')->setCalendarData(null);
      } elseif ( Mage::registry('calendar_data') ) {
          $form->setValues(Mage::registry('calendar_data')->getData());
      }
      return parent::_prepareForm();
  }
}