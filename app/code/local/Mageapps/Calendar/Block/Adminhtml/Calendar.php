<?php
class Mageapps_Calendar_Block_Adminhtml_Calendar extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_calendar';
    $this->_blockGroup = 'calendar';
    $this->_headerText = Mage::helper('calendar')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('calendar')->__('Add Item');
    parent::__construct();
  }
}